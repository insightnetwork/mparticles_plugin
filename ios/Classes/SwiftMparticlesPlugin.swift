import Flutter
import UIKit
import Foundation
import mParticle_Apple_SDK

public class SwiftMparticlesPlugin: NSObject, FlutterPlugin {
  var mParticleShared: MParticle?
  private var showOverlay = false
  private var channel: FlutterMethodChannel!
  
  enum FlutterMparticleKeys {
    case eventDescription, eventType, eventCategory, eventInfo

    var keys: String {
      switch self {
      case .eventDescription:
        return "eventDescription"
      case .eventType:
        return "eventType"
      case .eventCategory:
        return "eventCategory"
      case .eventInfo:
        return "eventInfo"
      }
    }
  }
  
  public static func register(with registrar: FlutterPluginRegistrar) {
    let instance = SwiftMparticlesPlugin()
    instance.channel = FlutterMethodChannel(name: "plugins.insighttimer.co/mparticles_plugin/channel", binaryMessenger: registrar.messenger())
    registrar.addMethodCallDelegate(instance, channel: instance.channel)
  }
  
  public func handle(_ call: FlutterMethodCall, result: @escaping FlutterResult) {
    var arguments = call.arguments as! [String: Any]
    switch call.method {
    case "Initialisation":
      do {
        let keyAndSecret = try getKeyAndSecret(arguments)
        let showOverlay = arguments["showOverlay"] as? Bool ?? false
        initialise(keyAndSecret.0, keyAndSecret.1, showOverlay: showOverlay)
      } catch {
        channel.invokeMethod("log", arguments: "mparticle key or secret is nil")
      }
    case "Log":
      guard let eventDescription = arguments[FlutterMparticleKeys.eventDescription.keys] as? String, let eventType = arguments[FlutterMparticleKeys.eventType.keys] as? String else {
        return
      }
      let eventCategory = arguments[FlutterMparticleKeys.eventCategory.keys] as? String
      let eventInfo = convertToDictionary(text: arguments[FlutterMparticleKeys.eventInfo.keys] as! String)
      logEvent(eventDescription: eventDescription, eventType: eventType, category: eventCategory, eventInfo: eventInfo)
    default:
      result(FlutterMethodNotImplemented)
      break
    }
  }
  
  func initialise(_ key: String, _ secret: String, showOverlay: Bool = false)  {
    self.showOverlay = showOverlay
    if mParticleShared == nil {
      mParticleShared = MParticle.sharedInstance()
    }
    let options = MParticleOptions(key: key, secret: secret)
    mParticleShared?.start(with: options)
  }
  
  func logEvent(eventDescription: String, eventType: String, category: String? = nil, eventInfo: [String: Any]? = nil) {
    guard let event =  MPEvent(name: eventDescription, type: Events(rawValue: eventType.lowercased())!.type()) else {
      return
    }
    if let category = category {
      event.category = category
    }
    if let eventInfo = eventInfo {
      event.info = eventInfo
    }
    do {
      let mParticleShared = try getParticle()
      mParticleShared.logEvent(event)
      if showOverlay {
        MParticleVisualDebugger.shared.show(event: event)
      }
    } catch {
      channel.invokeMethod("log", arguments: "MParticle not initialised properly.\nInitialisation method" + "shall be called before any log methods")
    }
  }
  
  private func getParticle() throws -> MParticle {
    guard let mParticleShared = self.mParticleShared else {
      throw MParticleError.invalid
    }
    return mParticleShared
  }
  
  private func getKeyAndSecret(_ arguments: [String: Any]) throws -> (String, String) {
    guard let key = arguments["mparticleKey"] as? String, let secret = arguments["mparticleSecret"] as? String else {
      throw MParticleError.invalid
    }
    return (key, secret)
  }
  
  private func convertToDictionary(text: String) -> [String: Any]? {
    if let data = text.data(using: .utf8) {
      do {
        return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
      } catch {
        print(error.localizedDescription)
      }
    }
    return nil
  }
}

extension SwiftMparticlesPlugin {
  
  enum Events: String {
    case unknown, navigation, location, search, transaction, usercontent, userpreference, social, other
    
    func type() -> MPEventType {
      switch self {
      case .unknown:
        return .click
      case .navigation:
        return .navigation
      case .location:
        return .location
      case .search:
        return .search
      case .transaction:
        return .transaction
      case .usercontent:
        return .userContent
      case .userpreference:
        return .userPreference
      case .social:
        return .social
      case .other:
        return .other
      }
    }
  }
}

enum MParticleError: Error {
  case invalid
}

#import "MparticlesPlugin.h"
#import <mparticles_plugin/mparticles_plugin-Swift.h>

@implementation MparticlesPlugin
+ (void)registerWithRegistrar:(NSObject<FlutterPluginRegistrar>*)registrar {
  [SwiftMparticlesPlugin registerWithRegistrar:registrar];
}

@end

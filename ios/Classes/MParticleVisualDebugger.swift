import UIKit
import Foundation
import mParticle_Apple_SDK

class MParticleVisualDebugger {
  static let shared = MParticleVisualDebugger()
  private lazy var textView = UITextView()
  private var counter: Int = 0
  
  func setupDebugWindowIfNeeded() {
    if textView.superview == nil {
      guard let keyWindow = UIApplication.shared.delegate?.window, let window = keyWindow else {
        return
      }
      textView.isUserInteractionEnabled = false
      textView.frame = window.frame
      textView.backgroundColor = UIColor.white.withAlphaComponent(0.4)
      textView.alpha = 0
      textView.textContainerInset = UIEdgeInsets(top: 100, left: 10, bottom: 50, right: 10)
      window.addSubview(textView)
    }
    if let keyWindow = UIApplication.shared.delegate?.window, let window = keyWindow {
      window.bringSubviewToFront(textView)
    }
  }
  
  func show(event: MPEvent) {
    let name = event.name
    var info = event.info?.map({ (key, value) -> String in
      return "\t\(key):\(value)"
    })
    info?.append("\ttype : \(event.typeName)")
    let combinedInfo = info?.joined(separator: ",\n")
    let fullString = combinedInfo.flatMap({ " {\n\($0)\n}" }) ?? ""
    show(text: name + fullString)
  }
  
  func show(text: String) {
    let newString = "\(self.counter): " + text + "\n"
    counter += 1
    
    DispatchQueue.main.async {
      self.setupDebugWindowIfNeeded()
      self.textView.text = newString + self.textView.text
      UIView.animate(withDuration: 0.2, delay: 0, options: [.beginFromCurrentState], animations: {
        self.textView.alpha = 1
      })
      DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
        var currentText = self.textView.text!
        if let range = currentText.range(of: newString) {
          currentText.replaceSubrange(range, with: "")
          self.textView.text = currentText
          if currentText.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines).count == 0 {
            UIView.animate(withDuration: 0.2, delay: 0, options: [.beginFromCurrentState], animations: {
              self.textView.alpha = 0
            })
          }
        }
      }
    }
  }
}

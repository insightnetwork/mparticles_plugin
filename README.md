# mparticles_plugin

mparticles plugin

## Getting Started

This plugin is used for sending mparticle events from Flutter.

It currently works for android but will be extended in the future for iOS as well.

The api is quite simple with only two methods:


/// For one-time initialisation, this shall be called once, preferably in main.dart.
/// The "showOverlay" is an optional argument, being 'false' by default. When 'true' it shows an
/// overlay (a toast in android) which shows the mparticle log being sent. This can be used in debug
/// builds for debugging purposes.
```initialise(String key, String secret, {bool showOverlay = false})```

###Examples:
```
Mparticle.initialise("some key", "some secret");
Mparticle.initialise("some key", "some secret", showOverlay: true);
```


/// This is the method used for logging any mparticle event.
/// EventType is an enum helping you choose the supported event types by Mparticles.
```logEvent(String eventDescription, EventType eventType, String eventCategory, Map<String, String> eventInfo)```

###Example:
```
Mparticle.logEvent(
                  "button_click",
                  EventType.Navigation,
                  null,
                  {"starting_page": "main", "button_name": "Click me"})
```


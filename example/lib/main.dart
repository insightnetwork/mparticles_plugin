import 'package:flutter/material.dart';
import 'package:mparticles_plugin/mparticles_plugin.dart';

void main() => runApp(MyApp());

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  void initState() {
    super.initState();
    Mparticle.initialise("some key", "some secret", showOverlay: true);
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: const Text('Mparticle plugin example app'),
        ),
        body: Center(
          child: RaisedButton(
              child: Text("Click me"),
              onPressed: () => Mparticle.logEvent(
                  "button_click",
                  EventType.Navigation,
                  null,
                  {"starting_page": "main", "button_name": "Click me"})),
        ),
      ),
    );
  }
}

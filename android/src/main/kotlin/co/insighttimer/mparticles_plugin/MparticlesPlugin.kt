package co.insighttimer.mparticles_plugin

import android.widget.Toast
import com.mparticle.MPEvent
import com.mparticle.MParticle
import com.mparticle.MParticleOptions
import io.flutter.plugin.common.MethodCall
import io.flutter.plugin.common.MethodChannel
import io.flutter.plugin.common.MethodChannel.MethodCallHandler
import io.flutter.plugin.common.MethodChannel.Result
import io.flutter.plugin.common.PluginRegistry.Registrar
import org.json.JSONException
import org.json.JSONObject
import timber.log.Timber

class MparticlesPlugin private constructor() : MethodCallHandler {
    private lateinit var registrar: Registrar
    private var showOverlay = false

    companion object {
        @JvmStatic
        fun registerWith(registrar: Registrar) {
            val channel = MethodChannel(registrar.messenger(), "plugins.insighttimer.co/mparticles_plugin/channel")
            channel.setMethodCallHandler(MparticlesPlugin(registrar))
        }
    }

    constructor(registrar: Registrar) : this() {
        this.registrar = registrar
    }

    override fun onMethodCall(call: MethodCall, result: Result) {
        val arguments = call.arguments as Map<String, Any>

        when (call.method) {
            "Initialisation" -> {
                val key = arguments["mparticleKey"] as String?
                val secret = arguments["mparticleSecret"] as String?
                if (key == null || secret == null) {
                    result.error("mparticle key or secret is null", null, null)
                    return
                }
                val showOverlay = arguments["showOverlay"] as Boolean? ?: false
                initialise(key, secret, showOverlay)
            }

            "Log" -> {
                val eventDescription = arguments[FlutterMparticleKeys.EventDescription.key] as String?
                val eventType = arguments[FlutterMparticleKeys.EventType.key] as String?
                if (!isInputValid(eventDescription, eventType, result = result)) return
                val eventCategory = arguments[FlutterMparticleKeys.EventCategory.key] as String?
                val eventInfoJsonString = arguments[FlutterMparticleKeys.EventInfo.key] as String?
                val eventInfo: Map<String, String?>? = eventInfoJsonString?.convertEventStringToMap()
                logEvent(eventDescription!!, eventType!!, eventCategory, eventInfo)
            }

            else -> result.notImplemented()
        }
    }

    private fun isInputValid(vararg inp: String?, result: Result): Boolean {
        var isValid: Boolean = true
        for (input in inp) {
            if (input == null) {
                isValid = false
                result.error("The $input is null", null, null)
            }
        }
        return isValid
    }

    private fun initialise(key: String, secret: String, showOverlay: Boolean) {
        this.showOverlay = showOverlay
        val mParticleOptions = MParticleOptions.builder(registrar.context())
                .credentials(key, secret).build()
        MParticle.start(mParticleOptions)
    }

    private fun logEvent(eventDescription: String, eventType: String, category: String? = null, eventInfo: Map<String, String?>? = null) {
        val mparticleEventType = makeMparticleEventType(eventType)
        val builder = MPEvent.Builder(eventDescription, mparticleEventType)
        category?.let {
            builder.category(it)
        }
        eventInfo?.let {
            builder.info(it)
        }
        MParticle.getInstance()?.logEvent(builder.build())
                ?: Timber.d("MParticle not initialised properly.\nInitialisation method" +
                        "shall be called before any log methods")
        val logMessage = "tracking mParticle event ===> name:[$eventDescription] type:[$mparticleEventType] category:[$category] info:[$eventInfo]"
        Timber.d(logMessage)
        if (showOverlay) Toast.makeText(registrar.context(), logMessage, Toast.LENGTH_LONG).show()
    }

    private fun String.convertEventStringToMap(): Map<String, String?>? {
        val map = mutableMapOf<String, String?>()
        val json = try {
            JSONObject(this)
        } catch (e: JSONException) {
            return null
        }
        for (key in json.keys()) {
            map[key] = json[key] as String?
        }
        return map
    }

    private fun makeMparticleEventType(eventType: String): MParticle.EventType {
        for (type in MParticle.EventType.values()) {
            if (eventType == type.name) return type
        }
        throw IllegalArgumentException("Invalid MParticle eventType $eventType from Flutter bridge")
    }

    enum class FlutterMparticleKeys(val key: String) {
        EventDescription("eventDescription"),
        EventType("eventType"),
        EventCategory("eventCategory"),
        EventInfo("eventInfo"),
    }
}

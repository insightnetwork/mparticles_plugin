import 'package:flutter/services.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mparticles_plugin/mparticles_plugin.dart';

void main() {
  const MethodChannel channel = MethodChannel('plugins.insighttimer.co/mparticles_plugin/channel');

  setUp(() {
    channel.setMockMethodCallHandler((MethodCall methodCall) async {
      return '42';
    });
  });

  tearDown(() {
    channel.setMockMethodCallHandler(null);
  });
}

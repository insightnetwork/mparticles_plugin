import 'dart:convert';

import 'package:flutter/services.dart';

class Mparticle {
  static const MethodChannel _channel =
      const MethodChannel('plugins.insighttimer.co/mparticles_plugin/channel');
  String _key, _secret;
  bool _showOverlay;
  static Mparticle _instance;

  Mparticle._();

  Mparticle.__(String key, String secret, showOverlay) {
    this._key = key;
    this._secret = secret;
    this._showOverlay = showOverlay;
    _channel.setMethodCallHandler(_handle);
    _instance = this;
  }

  /// This shall be called once for initialisation, preferably in the main.dart
  static initialise(String key, String secret, {bool showOverlay = false}) {
    assert(key != null && secret != null);
    _instance ?? Mparticle.__(key, secret, showOverlay);
    _channel.invokeMethod(_MparticleMethods._initialise, {
      _MparticleInitialisationKeys._key: key,
      _MparticleInitialisationKeys._secret: secret,
      _MparticleInitialisationKeys._showOverlay: showOverlay
    });
  }

  /// This shall be called for the actual logging. It throws an exception if the initialise
  /// method is called before.
  /// [eventCategory] and [eventInfo] are nullable.
  static logEvent(String eventDescription, EventType eventType,
      String eventCategory, Map<String, String> eventInfo) {
    assert(eventDescription != null && eventDescription.trim().length > 0);
    assert(eventType != null);
    if (_instance == null)
      throw Exception("The \"initialise\" method must be called first");
    _channel.invokeMethod(_MparticleMethods._log, {
      _MparticleKeys._eventDescription: eventDescription,
      _MparticleKeys._eventType: eventType.toString().split('.').last,
      _MparticleKeys._eventCategory: eventCategory,
      _MparticleKeys._eventInfo: _makeJsonStringFrom(eventInfo)
    });
  }

  static String _makeJsonStringFrom(Map<String, String> map) {
    return json.encode(map);
  }

  Future<dynamic> _handle(MethodCall call) async {
    switch (call.method) {
      case 'log':
        final String message = call.arguments as String;
        if (message != null) {
          print(message);
        }
        break;
      default:
        break;
    }
  }
}

enum EventType {
  Unknown,
  Navigation,
  Location,
  Search,
  Transaction,
  UserContent,
  UserPreference,
  Social,
  Other
}

class _MparticleInitialisationKeys {
  static const String _key = "mparticleKey";
  static const String _secret = "mparticleSecret";
  static const String _showOverlay = "showOverlay";
}

class _MparticleMethods {
  static const String _log = "Log";
  static const String _initialise = "Initialisation";
}

class _MparticleKeys {
  static const String _eventDescription = "eventDescription";
  static const String _eventType = "eventType";
  static const String _eventCategory = "eventCategory";
  static const String _eventInfo = "eventInfo";
}
